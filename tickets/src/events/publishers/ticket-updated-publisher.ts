import { Publisher, Subjects, TicketUpdatedEvent } from '@rostyslavp/common';

export class TicketUpdatedPublisher extends Publisher<TicketUpdatedEvent> {
  subject: Subjects.TicketUpdated = Subjects.TicketUpdated;
}
