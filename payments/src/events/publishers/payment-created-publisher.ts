import { Subjects, Publisher, PaymentCreatedEvent } from '@rostyslavp/common';

export class PaymentCreatedPublisher extends Publisher<PaymentCreatedEvent> {
  subject: Subjects.PaymentCreated = Subjects.PaymentCreated;
}
