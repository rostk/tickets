import { Subjects, Publisher, OrderCancelledEvent } from '@rostyslavp/common';

export class OrderCancelledPublisher extends Publisher<OrderCancelledEvent> {
  subject: Subjects.OrderCancelled = Subjects.OrderCancelled;
}
