import {
  Subjects,
  Publisher,
  ExpirationCompleteEvent,
} from '@rostyslavp/common';

export class ExpirationCompletePublisher extends Publisher<
  ExpirationCompleteEvent
> {
  subject: Subjects.ExpirationComplete = Subjects.ExpirationComplete;
}
